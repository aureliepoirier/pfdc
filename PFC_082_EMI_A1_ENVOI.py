# ============================================================================= #
#        ______  ________    ______  ________  ______   _____  ____    ____     #
#      .' ___  ||_   __  | .' ___  ||_   __  ||_   _ `.|_   _||_   \  /   _|    #
#     / .'   \_|  | |_ \_|/ .'   \_|  | |_ \_|  | | `. \ | |    |   \/   |      #
#     | |         |  _| _ | |   ____  |  _| _   | |  | | | |    | |\  /| |      #
#     \ `.___.'\ _| |__/ |\ `.___]  |_| |__/ | _| |_.' /_| |_  _| |_\/_| |_     #
#      `.____ .'|________| `._____.'|________||______.'|_____||_____||_____|    #
#   ________        ______                    _                                 #
#  |_   __  |      |_   _ \                  (_)                                #
#    | |_ \_| ______ | |_) | __   _   .--.   __   _ .--.  .---.  .--.   .--.    #
#    |  _| _ |______||  __'.[  | | | ( (`\] [  | [ `.-. |/ /__\\( (`\] ( (`\]   #
#   _| |__/ |       _| |__) || \_/ |, `'.'.  | |  | | | || \__., `'.'.  `'.'.   #
#  |________|      |_______/ '.__.'_/[\__) )[___][___||__]'.__.'[\__) )[\__) )  #
#																			    #
# ============================================================================= #
# coding: utf-8 											        Version 1.0 #
# ============================================================================= #
#                             Traitement spécifique                             #
# ============================================================================= #
#   Date de création : 16/02/2017                                               #
#	Authors :	       auteur@cegedim.fr 		     	 	 		            #
# ============================================================================= #

import time
import shutil
import glob
import os
import ctools

# Traitement lors de la premiere execution de la journee
def first():

	return True

# Traitement pre traduction
def pre():

	# Enveloppage des fichiers
	logger.info("Enveloppage des fichiers")
	for file in glob.glob(WorkingDirectory + "*.edi"):
		evl = ctools.Enveloppe(tmsg="A1", idtrait="ENVOI")
		# On fixe à la main les informations de l'enveloppe
		evl.cre = "082"
		evl.crd = "ATL"
		evl.date = time.strftime("%Y-%m-%d")
		evl.hour = time.strftime("%H.%M.%S")
		evl.refint = OpconSessionID
		evl.tmsg = "A1"
		evl.idtrait = "ENVOI"
		evl.filepath = file
		evl.size = str(os.path.getsize(file)).zfill(10)
		evl._evlpath = file.rsplit (".",1)[0] + ".evl"
		evl.createEvlFile()

	return True

# Traitement post traduction
def post():

	return True

# Traitement lors de la derniere execution de la journee
def last():

	return True